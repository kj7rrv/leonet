import hmac
import hashlib
import base64


def find_auth_code(password, message):
    if password:
        return base64.b64encode(
            hmac.digest(
                password.encode("utf-8"),
                message.encode("ascii"),
                "sha256",
            )
        ).decode("ascii")
    else:
        return ""


def find_checksum(data):
    return base64.b64encode(
        hashlib.sha256(data.encode("ascii")).digest()[:8]
    ).decode("ascii")
