import base64


def encode(message):
    return "B64:" + base64.b64encode(message.encode("utf-8")).decode("ascii")


def decode(message):
    code, content = message.split(":", 1)
    if code == "B64":
        return base64.b64decode(content).decode("utf-8")
