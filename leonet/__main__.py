import os
import time
import threading
import argparse

import modem
import frames


sent_messages = {}
received_messages = {}


def sends():
    while True:
        try:
            with open(input_file) as f:
                recipient, message = f.read().split(" ", 1)
            os.unlink(input_file)
        except FileNotFoundError:
            continue

        message = message.strip()

        if message.startswith("QRY "):
            message_id = message.split(" ")[1]
            modem.send(
                modem.wrap_frame(
                    frames.QryFrame(
                        recipient, my_identifier, message_id
                    ).encode(),
                    callsign,
                )
            )
        else:
            frame = frames.MsgFrame(
                recipient,
                my_identifier,
                message,
            )

            sent_messages[frame.message_id] = [
                frame,
                False,
            ]
            modem.send(modem.wrap_frame(frame.encode(), callsign))


parser = argparse.ArgumentParser(prog="leonet")
parser.add_argument("identifier", help="my identifier (e.g. CALLSIGN-1)")
parser.add_argument(
    "callsign", help="my call sign (does not need to match identifier)"
)
parser.add_argument("input_file", help="file to watch for messages")
args = parser.parse_args()

my_identifier = args.identifier
callsign = args.callsign
input_file = args.input_file

threading.Thread(target=sends).start()

transceiver = modem.Transceiver(my_identifier, callsign)

while True:
    frame = transceiver.receive()
    print()
    print(frame)
    if isinstance(frame, frames.MsgFrame):
        print(frame.timestamp, frame.originator, frame.message)
        time.sleep(5)
        received_messages[frame.message_id] = frame
        transceiver.transmit(frames.gen_ack().encode())
    elif isinstance(frame, frames.QryFrame):
        time.sleep(5)
        if frame.message_id in received_messages:
            transceiver.transmit(frames.gen_ack().encode())
        else:
            transceiver.transmit(frames.gen_nak().encode())
